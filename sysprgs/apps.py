from django.apps import AppConfig


class SysprgsConfig(AppConfig):
    name = 'sysprgs'
